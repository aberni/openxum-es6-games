import AI from '../lib/openxum-ai/index.mjs';
import OpenXum from '../lib/openxum-core/index.mjs';

/*
let win = { 'Joueur 1': 0, 'Joueur 2': 0 };
for (let i = 0; i < 3; ++i) {
  let e = new OpenXum.Dakapo.Engine();
  let p1 = new AI.Specific.Dakapo.MCTSPlayer('Joueur 1', 'Joueur 2', e);
  let p2 = new AI.Specific.Dakapo.AlphaBetaPlayer('Joueur 2', 'Joueur 1', e);
  let p = p1;

  while (!e.is_finished()) {
    let move = p.move();

    e.move(move);
    p = p === p1 ? p2 : p1;
  }
  ++win[e.winner_is()];
}

console.log("Random: " + win['Joueur 2'] + " wins");
console.log("MCTS: " + win['Joueur 1'] + " wins");
*/
let player1_win = 0;
let player2_win = 0;
let player1 = OpenXum.Dakapo.Player.PLAYER_1;
let player2 = OpenXum.Dakapo.Player.PLAYER_2;
let e;
let p1;
let p2;

function getNameOfAI(player) {
  if(player === undefined) {
    return "undefined";
  }
  if(player instanceof AI.Generic.RandomPlayer) {
    return "Random";
  }
  if(player instanceof AI.Specific.Dakapo.MCTSPlayer) {
    return "MCTS";
  }
  if(player instanceof  AI.Specific.Dakapo.AlphaBetaPlayer) {
    return "AlphaBetaPlayer";
  }
  return "Other";
}

for (let i = 0; i < 10; ++i) {
  e = new OpenXum.Dakapo.Engine();
  p1 = new AI.Specific.Dakapo.AlphaBetaPlayer(player1, player2, e);
  p2 = new AI.Specific.Dakapo.MCTSPlayer(player2, player1, e);

  let p = p1;
  while (!e.is_finished()) {
    let startTime = new Date();
    let move = p.move();

    let endTime = new Date();
    if(e.current_color() === player1) {
      console.log("Player1's choice: " + move.to_string() +" ("+(endTime - startTime)/1000 + " s) "+getNameOfAI(p));
    } else {
      console.log("Player2's choice: " + move.to_string() +" ("+(endTime - startTime)/1000 + " s) "+getNameOfAI(p));
    }

    e.move(move);
    p = p === p1 ? p2 : p1;
  }

  let winner = e.winner_is();
  if(winner === player1) {
    console.log("Winner is Player1 ("+getNameOfAI(p1)+")");
    player1_win++;
  } else if(winner === player2){
    console.log("Winner is Player2 ("+getNameOfAI(p2)+")");
    player2_win++;
  } else {
    console.log("Draw");
  }
}

console.log("Player1: " + player1_win + " wins (" + getNameOfAI(p1) + ")");
console.log("Player2: " + player2_win + " wins (" + getNameOfAI(p2) + ")");