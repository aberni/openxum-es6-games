"use strict";

import AlphaBeta from '../../generic/alphabeta_player.mjs';

class AlphaBetaPlayer extends AlphaBeta {
  constructor(c, o, e) {
    super(c, o, e, 3, 1000);
  }

  evaluate(e, depth) {
    // If the game is finished, return final score
    if(e.is_finished()) {
      switch(e.winner_is()) {
        case this.color():
          return this.VICTORY_SCORE - depth;
        case this.opponent_color():
          return depth - this.VICTORY_SCORE;
        default:
          return 0;
      }
    }
    return 0;
  }
}

export default AlphaBetaPlayer;