"use strict";

import MCTSPlayer from './mcts_player.mjs';
import AlphaBetaPlayer from "./alphabeta_player";

export default {
  MCTSPlayer: MCTSPlayer,
  AlphaBetaPlayer: AlphaBetaPlayer
};